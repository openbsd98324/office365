
The Evolution Mail client supports Office 365 through the evolution-ews plugin, starting in evolution-ews v3.27.91 and above. The plugin provides Exchange Web Services and modern authentication support, required to access Office 365 mail with Okta MFA. Ubuntu 18.04 and Fedora 28 are known to be working, although any Linux distribution with evolution-ews v3.27.91 or above in it's package repository should theoretically work.

To install and configure Evolution on RHEL or Fedora Core 28,



Evolution on Devuan ASCII (ascii release, like buster) is  3.22.6
Office 365 does not works with ews.
````
ii  evolution                              3.22.6-1+deb9u2                            amd64        group
ware suite with mail client and organizer
ii  evolution-common                       3.22.6-1+deb9u2                            all          architecture independent files for Evolution
ii  evolution-data-server                  3.22.7-1                                   amd64        evolution database backend server
ii  evolution-data-server-common           3.22.7-1                                   all          architecture independent files for Evolution Data Server
ii  evolution-ews                          3.22.6-1                                   amd64        Exchange Web Services integration for Evolution
ii  evolution-plugins                      3.22.6-1+deb9u2                            amd64        standard plugins for Evolution
ii  libebackend-1.2-10:amd64               3.22.7-1                                   amd64        Utility library for evolution data servers
ii  libebook-1.2-16:amd64                  3.22.7-1                                   amd64        Client library for evolution address books
ii  libebook-contacts-1.2-2:amd64   
````


Evolution on Devuan Chimaera (chimaera release, like ...) is  ....
Office 365 works with ews.
````
````